import requests

url = "http://localhost:8088/rest/raven/1.0/import/feature?projectKey=RWIL"

payload = {}
files = [
  ('file', open('features.zip','rb'))
]
headers = {
  'Authorization': 'Basic cGhpbGlwcC53b2xmOmtpdHpsaWNo',
  'Cookie': 'JSESSIONID=C5D759E0C25D367A7A2530395B02560C; atlassian.xsrf.token=BVYV-N86P-4NCA-9XP2_7b82ee8cdb246c9cd692c01b0e147de39ba22613_lin'
}

response = requests.request("POST", url, headers=headers, data = payload, files = files)

print(response.text.encode('utf8'))
