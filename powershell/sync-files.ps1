try {
 $source = "source/src/test/features"

 $destination = "features.zip"

 If (Test-path $destination)
 {
  Remove-item $destination
 }

 Add-Type -assembly "system.io.compression.filesystem"
 Add-Type -assembly "system.net.http"

 [io.compression.zipfile]::CreateFromDirectory($source, $destination)

} catch {
 write-host $_.Exception.Message
}