echo "Generating feature files zip"
export FEATURES=$(git diff --name-only $CI_COMMIT_BEFORE_SHA $CI_COMMIT_SHA | grep ".feature")
if [ -n "$FEATURES" ]; then
  apt-get update -qq -y
  apt-get -install zip -y
  echo "$FEATURES" | xargs zip -r features.zip

  curl -H "Content-Type: multipart/form-data" -X pOST -k -u "PWolfSP:Kitzlich3" -F "file=Qfeatures.zip"
else
  echo "No Features found!"
fi

echo "done"